const venom = require('venom-bot');
const axios = require('axios').default;

const path = require('path');
const {google} = require('googleapis');
const privatekey = require("./credentials.json");
//const {authenticate} = require('@google-cloud/local-auth');

const sheets = google.sheets('v4');

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.

const dataSpreadsheetId = "1tWwRUjLPlTv6o48IVVUwvjeBasHwaIB5-QcMId5n7B8";

//TODO store in localstorage
let chatState = {};

//TODO get from spreadsheet
let languageList = {"en":"English", "gu":"Gujarati", "hi":"Hindi"};
//TODO use default vaues in case no connection with database
let helpTriggers;
//TODO use default vaues in case no connection with database
let languageTriggers = {};
//TODO use default vaues in case no connection with database
let menuItems = {};
//TODO use default vaues in case no connection with database
let commonMessages = {};

let dataDict = {};

let lastUpdateTime = 0;

const fixedStrings = {
    'invalid_sel' : {
        'en' : "Invalid selection",
        'gu' : "અમાન્ય પસંદગી",
        'hi' : "अमान्य चयन"
    },
    'enter_sel' : {
        'en' : "Please enter your selection",
        'gu' : "કૃપા કરીને તમારી પસંદગી દાખલ કરો",
        'hi' : "कृपया अपना चयन दर्ज करें"
    },
    'contact' : {
        'en' : "Contact Numbers:",
        'gu' : "સંપર્ક નંબર:",
        'hi' : "संपर्क संख्या:"
    },
    'address' : {
        'en' : "Address:",
        'gu' : "સરનામું:",
        'hi' : "पता:"
    },
    'map' : {
        'en' : "Maps Link:",
        'gu' : "નકશાની લિંક:",
        'hi' : "मानचित्र लिंक:"
    },
    'note' : {
        'en' : "Please Note:",
        'gu' : "નોંધ:",
        'hi' : "कृपया ध्यान दें:"
    },
    'no_data' : {
        'en' : "No data for ",
        'gu' : " માટે કોઈ ડેટા નથી",
        'hi' : " के लिए कोई डेटा नहीं"
    },
    'using_lang' : {
        'en' : "Using Language: ",
        'gu' : "ભાષા: ",
        'hi' : "भाषा: "
    },
    'back' : {
        'en' : "Back",
        'gu' : "પાછા",
        'hi' : "वापस"
    },
    'help_again' : {
        'en' : "Send ' *help* ' again to get new information",
        'gu' : "નવી માહિતી મેળવવા માટે ફરીથી ' *help* ' મોકલો ",
        'hi' : "नई जानकारी प्राप्त करने के लिए फिर से ' *help* ' भेजें"
    },
    'selection_help' : {
        'en' : "Please send the *number* from the menu. For example, enter *2* for reports like CT Scan or RTPCR",
        'gu' : "કૃપા કરી મેનુમાંથી *નંબર* મોકલો. ઉદાહરણ તરીકે, સીટી સ્કેન અથવા આર​.ટી.પી.સી.આર જેવા રિપોર્ટ માટે *2* દાખલ કરો ",
        'hi' : "कृपया मेनू से *नंबर* भेजें। उदाहरण के लिए, CT स्कैन या RTPCR जैसी रिपोर्ट के लिए *2* दर्ज करें"
    }
};

function init() {
    //Load client secrets from a local file.
    
    // configure a JWT auth client
    let auth = new google.auth.JWT(
        privatekey.client_email,
        null,
        privatekey.private_key,
        ['https://www.googleapis.com/auth/spreadsheets']);
    //authenticate request
    auth.authorize(function (err, tokens) {
        if (err) {
        console.log(err);
        return;
        } else {
        console.log("Successfully connected!");
        }
    });
//     const auth = await google.auth.getClient({
//         keyfilePath: path.join(__dirname, 'credentials.json'),
//         scopes: ['https://www.googleapis.com/auth/spreadsheets'],
//     });
    google.options({auth});
    
    getDataFromSheet(true);
}

function getDataFromSheet(ovrride=false) {
    var f = false;
    if(ovrride) {
        f = true;
    }
    else {
        if(((+ new Date()) - lastUpdateTime) > (3600000))
            f = true;
    }
    //TODO define the interval on top
    if(f) {
        getMenu();
        getHelpTriggers();
        getLanguageTriggers();
        getGeneralMessages();
        extractData();
        lastUpdateTime = + new Date();
    }
}

/**
 * Prints the names and majors of students in a sample spreadsheet:
 * @see https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function getMenu() {
  sheets.spreadsheets.values.get({
    spreadsheetId: dataSpreadsheetId,
    //TODO define on top
    range: 'menuItems!A1:D',
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    var rows = res.data.values;
    
    if (rows.length) {
      console.log('parsing menu from google spreadsheet');
      
      var obj = {};
      header = rows[0];
      rows = rows.slice(1,);
      rows.map((row) => {
        //TODO make it generic
        if(row[0] !== undefined && row[1] !== undefined && row[2] !== undefined && row[3] !== undefined) {
            obj[row[0]] = {};
            obj[row[0]][header[1]] = row[1].split(',');
            trimArray(obj[row[0]][header[1]]);
            obj[row[0]][header[2]] = row[2].split(',');
            trimArray(obj[row[0]][header[2]]);
            obj[row[0]][header[3]] = row[3].split(',');
            trimArray(obj[row[0]][header[3]]);
            //console.log(`${row[0]}, ${row[1]}, ${row[2]}`);
        }
      });
      menuItems = obj;
      //console.log(obj);

    } else {
      console.log('No menu data found.');
    }
  });
}

function getHelpTriggers() {
  sheets.spreadsheets.values.get({
    spreadsheetId: dataSpreadsheetId,
    //TODO define on top
    range: 'triggers!A2:B',
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    var rows = res.data.values;
    
    if (rows.length) {
      console.log('parsing help from google spreadsheet');
      
      var obj = {};
      //header = rows[0];
      //rows = rows.slice(1,);
      rows.map((row) => {
        if(row[0] !== undefined && row[1] !== undefined) {
            obj[row[0]] = row[1].trim();
            //console.log(`${row[0]}, ${row[1]}, ${row[2]}`);
        }
      });
      //console.log(obj);
      helpTriggers = obj;

    } else {
      console.log('No help triggers data found.');
    }
  });
}

function getLanguageTriggers() {
  sheets.spreadsheets.values.get({
    spreadsheetId: dataSpreadsheetId,
    //TODO define on top
    range: 'triggers!D2:E',
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    var rows = res.data.values;
    
    if (rows.length) {
      console.log('parsing language triggers from google spreadsheet');
      
      var obj = {};
      rows.map((row) => {
        //TODO make it generic
        if(row[0] !== undefined && row[1] !== undefined) {
            obj[row[0]] = row[1].split(',');
            trimArray(obj[row[0]]);
            //console.log(`${row[0]}, ${row[1]}, ${row[2]}`);
        }
      });
      languageTriggers = obj;
      //console.log(obj);

    } else {
      console.log('No language triggers data found.');
    }
  });
}

function getGeneralMessages() {
  sheets.spreadsheets.values.get({
    spreadsheetId: dataSpreadsheetId,
    //TODO define on top
    range: 'General messages!A1:D',
  }, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    var rows = res.data.values;
    
    if (rows.length) {
      console.log('parsing general messages from google spreadsheet');
      
      var obj = {};
      header = rows[0];
      rows = rows.slice(1,);
      rows.map((row) => {
        //TODO make it generic
        if(row[0] !== undefined && row[1] !== undefined) {
            obj[row[0]] = {};
            obj[row[0]][header[1]] = row[1].trim();
            obj[row[0]][header[2]] = row[2].trim();
            obj[row[0]][header[3]] = row[3].trim();
            //console.log(`${row[0]}, ${row[1]}, ${row[2]}`);
        }
      });
      commonMessages = obj;
      //console.log(obj);

    } else {
      console.log('No general message data found.');
    }
  });
}

function trimArray(arr) {
    for(var i = 0; i < arr.length; i++) {
        arr[i] = arr[i].trim();
    }
    return arr;
}

function parseDataFromSheet(rows) {
    header = rows[0];
    rows = rows.slice(2,);
    
    var flag = true;
    //find index of subcategory
    var catI = header.indexOf('subcategory');
    if(catI < 0)
        flag = false;
    //find index of name
    var namI = {};
    header.forEach((v,i) => {
        if(v.trim().startsWith('name_')) {
            namI[i] = v.trim().substring('name_'.length, );
        }
    });
    if(Object.keys(namI).length <= 0)
        flag = false;
    //find index of address
    var addI = {};
    header.forEach((v,i) => {
        if(v.trim().startsWith('address_')) {
            addI[i] = v.trim().substring('address_'.length, );
        }
    });
    if(Object.keys(addI).length <= 0)
        flag = false;
    //find index of contact
    var conI = header.indexOf('contact');
    if(catI < 0)
        flag = false;
    //find index of map link
    var mapI = header.indexOf('map link');
    //find index of notes
    var notI = header.indexOf('notes');
    if(flag) {
        rows.map((row) => {
            if(row[0] !== undefined && row[0] !== '') {
                //console.log(row[0]);
                var categories = row[catI].split(',');
                trimArray(categories);
                //console.log(categories);
                var curData = {"name": {}, "address": {}};
                //name address
                for (const [key, value] of Object.entries(namI)) {
                    if(row[key] === undefined) {
                        curData['name'][value] = '';
                    }
                    else {
                        curData['name'][value] = row[key];
                    }
                    //console.log(key, value);
                }
                
                if(row[conI] === undefined) {
                    curData["contact"] = [];
                }
                else {
                    curData["contact"] = trimArray(row[conI].split(','));
                }
                
                for (const [key, value] of Object.entries(addI)) {
                    if(row[key] === undefined) {
                        curData['address'][value] = '';
                    }
                    else {
                        curData['address'][value] = row[key];
                    }
                    //console.log(key, value);
                }
                
                if(row[mapI] === undefined) {
                    curData["mapLink"] = '';
                }
                else {
                    curData["mapLink"] = row[mapI];
                }
                
                if(row[notI] === undefined) {
                    curData["notes"] = '';
                }
                else {
                    curData["notes"] = row[notI];
                }
                
                categories.forEach((cat) => {
                if(!(Object.keys(dataDict).includes(cat))) {
                        dataDict[cat] = [];
                    }
                    dataDict[cat].push(curData);
                });
            }
        });
    }
}

function extractData() {
    var i;
    sheets.spreadsheets.get({
        spreadsheetId: dataSpreadsheetId
    },(err, res) => {
        if (err) return console.log('The API returned an error: ' + err);
        sheetList = res.data.sheets;
        dataDict = {};
//         console.log(sheetList);
        for(i = 0; i < sheetList.length; i++) {
            sheet = sheetList[i].properties.title;
            console.log(sheet);
            sheets.spreadsheets.values.get({
                spreadsheetId: dataSpreadsheetId,
                range: sheet,
            }, (err2,res2) => {
                if (err2) return console.log('The API returned an error: ' + err2);
                var rows;
                if(!('values' in res2.data))
                    rows = [];
                else
                    rows = res2.data.values;
                if (rows.length > 0) {
                    console.log('parsing data from '+res2.data.range);
                    parseDataFromSheet(rows);
                } else {
                    console.log('No data found in '+res2.data.range);
                }
            });
        }
    });
    
    //console.log(res.data.sheets[0].properties.title);
    
    //console.log(Object.keys(dataDict));
    //console.log(dataDict['Ambulance With ICU']);
}

venom
    .create()
    .then((client) => start(client))
    .catch((erro) => {
        console.log(erro);
    });
 
init();

function sendMessage(client, to, msg, del=false) {
    //     console.log("tt sending to: "+ to + " - "+msg);
    //     console.log("\n"+client);
    client.sendText(to, msg)
        .then((result) => {
            console.log('message sent ' + msg + ' to ' + to); //return object success
            if(del) {
                chatState[to]['del'].push(result['to']['_serialized']);
            }
            //console.log(chatState[to]['del']);
            return 0;
        })
        .catch((erro) => {
            console.error('Error when sending: ', erro); //return object error
            return -1;
        });
}

function sendMessageDelayed(client, to, msg, del=false, delayMs) {
    //     console.log("tt sending to: "+ to + " - "+msg);
    //     console.log("\n"+client);
    setTimeout(() => {sendMessage(client,to,msg,del);}, delayMs);
}

function sendFile(client, to, fil) {
    client.sendFile(to,fil,'','')
    .then((result) => {
        //console.log('Result: ', result); //return object success
    })
    .catch((erro) => {
        console.error('Error when sending image: ', erro); //return object error
    });
}

function indexOfHelpTrigger(str) {
    var i = 0;
    for(const [key, value] of Object.entries(helpTriggers)) {
        if(str.toLowerCase().includes(key)) {
            return value;
        }
    }
    return -1;
}

function indexOfLanguageTrigger(str) {
    for(const [key, value] of Object.entries(languageTriggers)) {
        if(value.indexOf(str.toLowerCase()) > -1) {
            return key;
        }
    }
    return -1;
}
function sendHelpMessage(client, usr) {
    var str2 = "Information for Rajkot only\n\nગુજરાતી માટે, 'GJ' મોકલો\nहिंदी के लिए, 'HIN' भेजिए\nFor English, send 'EN'";
            sendMessage(client, usr, str2, true);
}

function deleteExtraMessages(client, usr) {
    if(chatState[usr]['del'].length > 0) {
            client.deleteMessage(usr, chatState[usr]['del'],)
            .then((result) => {
                //console.log('Result: ', result); //return object success
                console.log('extra messages deleted');
                chatState[usr]['del'] = [];
            })
            .catch((erro) => {
                console.error('Error when deleting: ', erro); //return object error
            });
        }
}

function sendMenu(client, usr) {
    //console.log("menu call "+chatState[usr]['state']);
    if(chatState[usr]['state'] === 1) {
        var temp = chatState[usr];
        var item = temp['selection'][temp['selection'].length - 1];
        //menu of the last selection in slected language
        var menuList = menuItems[item][temp['lang']];
        var menuListEn = menuItems[item]['en'];
        
        //deleteExtraMessages(client, usr);
        
        console.log("sending menu");
        if(item === 'main') {
            sendHelpMessage(client, usr);
        }
        var str = '';
        var i = 0;
        for(i = 0; i < menuList.length; i++) {

            str += ((i + 1).toString() + " - " + menuList[i]);
            if(Object.keys(menuItems).indexOf(menuListEn[i]) > -1) {
                str += "*";
            }
            str += '\n';
        }
        if(item !== 'main') {
            str += ((i + 1).toString() + " - "+fixedStrings['back'][temp['lang']]+"\n");
            i++;
        }
        //TODO add back option
        str += fixedStrings['enter_sel'][temp['lang']]+" (1-"+i+"):";

        sendMessageDelayed(client, usr, str, true, 300);
        if(item === 'main') {
            sendMessageDelayed(client, usr, fixedStrings['selection_help'][temp['lang']], true, 1000);
        }
    }
}

function sendData(client, usr) {
    if(chatState[usr]['state'] === 2) {
        var temp = chatState[usr];
        //menu of the last selection in slected language
        var item = temp['selection'][temp['selection'].length - 1];
        var ti = menuItems[temp['selection'][temp['selection'].length - 2]]['en'].indexOf(item);
        var itemLang = menuItems[temp['selection'][temp['selection'].length - 2]][temp['lang']][ti];
        console.log("sending data");
        
        //deleteExtraMessages(client, usr);
        
        // TODO add feedback logic using another state
        console.log('/contact?sub='+item+'&no='+usr.slice(0, -5));
        
        sheets.spreadsheets.values.append({
            spreadsheetId: dataSpreadsheetId,
            //TODO define on top
            range: 'queryLog!A2:D',
                    valueInputOption: 'USER_ENTERED',
                    requestBody: {
                    values: [
                        [usr.slice(0, -5), item, temp['lang'], + new Date()]
                    ],
            }}, (err, res) => {
            if (err) return console.log('The API returned an error: ' + err);
            console.log(res.data);
        });
        
        if((menuItems['General Information']['en'].indexOf(item) > -1) && item !== 'Donations') {
            if(item === 'Action Plan') {
                sendFile(client,usr,'files/action_plan.jpg');
            }
            else if(item === 'Mucormycosis(Black Fungus)') {
                sendFile(client,usr,'files/mucormycosis.pdf');
            }
            else if(item === 'Self Care') {
                sendFile(client,usr,'files/self_care_'+temp['lang']+'.png');
                sendFile(client,usr,'files/self_care_2.pdf');
            }
            else if(item === 'Awareness Information') {
                sendFile(client,usr,'files/awareness_1.jpg');
                sendFile(client,usr,'files/awareness_2.jpg');
                sendFile(client,usr,'files/awareness_3.jpg');
                sendFile(client,usr,'files/awareness_4.jpg');
                sendFile(client,usr,'files/FAQ.pdf');
            }
            else if(item === 'FAQs') {
                sendFile(client,usr,'files/FAQ.pdf');
            }
            else if(item === 'Breathing Techniques') {
                client.sendLinkPreview(usr,'https://www.youtube.com/playlist?list=PLcecSC9-dGKDUnjJpJgetPZsGZC6YYkk9','','')
                    .then((result) => {
                        //console.log('Result: ', result); //return object success
                    })
                    .catch((erro) => {
                        console.error('Error when sending image: ', erro); //return object error
                    });
            }
            else if(item === 'Vaccination') {
                if(temp['lang'] === 'gu') {
                    sendFile(client,usr,'files/vaccination_gu.pdf');
                }
                else {
                    sendFile(client,usr,'files/vaccination.pdf');
                }
                sendFile(client,usr,'files/vaccination.jpeg');
                sendFile(client,usr,'files/vaccination_scam.jpeg');
                client.sendLinkPreview(usr,'https://selfregistration.cowin.gov.in/','','')
                    .then((result) => {
                        //console.log('Result: ', result); //return object success
                    })
                    .catch((erro) => {
                        console.error('Error when sending image: ', erro); //return object error
                    });
                sendMessage(client,usr,'Rajkot 18-44 vaccine slot alert-telegram bot https://t.me/rajkot1844');
            }
            else if(item === 'Provide Leads' || item === 'Suggestions or Feedback') {
                sendMessage(client,usr,'*Your next message will be saved for our team to process, please write everything within single message*');
                if(item === 'Provide Leads') {
                    sendMessage(client,usr,"Format of message\n\nName of resource:\nContact:\nLocation:\nServices provided:");
                }
                chatState[usr]["state"] = 3;
            }
            else if(item === 'Contributors') {
                sendMessage(client,usr,'Entire Project:\nRavi\nAlihusen (https://twitter.com/AlihusenK)\nAsmita\n\nData Contributors:\nDhaval Beladiya\nRaj Mehta\nDeep Parmar\nMany more from Rajkot Covid Seva Group Volunteers (https://instagram.com/covid_help_rajkot)\nMadad Group (https://twitter.com/CovidMadad)\nhttps://twitter.com/trivedi_ravi13\nhttps://twitter.com/ket_ec\nhttps://twitter.com/rutultrivedi\nhttps://twitter.com/vanditdholakia\nhttps://twitter.com/IamNDMakwana\nhttps://twitter.com/JAYPANDYA09\n\nDevelopment:\nRishav (http://github.com/rishavt)\nRavindra Bosamiya[WhatsApp bot]\n\nGraphics:\nDeep Parmar\n\nTesting:\nSmit (https://www.linkedin.com/in/smit-falia-320b2074)\nDharin\n\nTranslations:\nAlihusen\n\nDevice:\nJay');
            }
            else if(item === 'Whatsapp Groups') {
                sendMessage(client,usr,"https://chat.whatsapp.com/KPaZIoUM5LpClrMBlEyqSM\n\nhttps://chat.whatsapp.com/LQGIqeNCqzMDcCXxeB9XMQ\n\nhttps://chat.whatsapp.com/DqGGuyYgsXs4kQ2fCnjluK\n\nhttps://chat.whatsapp.com/IAM5Jw43jG20qWXatWsPEn\n\nhttps://chat.whatsapp.com/HoXQGiRHsdw3Rvcy6FKMcv\n\nIn case information is not sufficient, please join *any one* group. *Join single group* so that they don't become full.");
            }
            else {
                if(temp['lang'] === 'en')
                    var str = fixedStrings['no_data'][temp['lang']] + itemLang;
                else
                    var str = itemLang + fixedStrings['no_data'][temp['lang']];
                sendMessage(client, usr, str);
                setTimeout(() => {sendMenu(client, usr)}, 500);
            }
            sendMessageDelayed(client,usr,fixedStrings['help_again'][temp['lang']], true, 35000);
        }
        else {
            getDataFromSheet();
            if(item in dataDict)
                contacts = dataDict[item];
            else 
                contacts = [];
            console.log('contact');
            //console.log(contacts);
            if(item === 'Amphoterecin') {
                sendFile(client,usr,'files/amphotericin_rmc.jpeg');
            }
            else if(item === 'Remdesivir' || item === 'Tocilizumab') {
                sendFile(client,usr,'files/remdesivir_rmc.jpeg');
            }
            if(contacts.length > 0){
                contacts.forEach((c) => {
                    var str = '';
                    if(chatState[usr]['lang'] in c['name'] && c['name'][chatState[usr]['lang']] !== '') {
                        str += c['name'][chatState[usr]['lang']];
                    }
                    else {
                        str += c['name']['en'];
                    }
                    if('contact' in c && c['contact'].length > 0) {
                        str += "\n"+fixedStrings['contact'][temp['lang']]+"\n";
                        c['contact'].forEach((n) => {
                            if(n !== '')
                                str += n+', ';
                        });
                        str = str.slice(0, -2);
                    }
                    if(chatState[usr]['lang'] in c['address'] && c['address'][chatState[usr]['lang']] !== '') {
                        str += "\n"+fixedStrings['address'][temp['lang']]+"\n";
                        str += c['address'][chatState[usr]['lang']];
                    }
                    else if(c['address']['en'] !== ''){
                        str += "\n"+fixedStrings['address'][temp['lang']]+"\n";
                        str += c['address']['en'];
                    }
                    
                    if('mapLink' in c && c['mapLink'] !== '') {
                        str += "\n"+fixedStrings['map'][temp['lang']]+"\n";
                        str += c['mapLink'];
                    }
                    if('notes' in c && c['notes'] !== '') {
                        str += "\n"+fixedStrings['note'][temp['lang']]+"\n";
                        str += c['notes'];
                    }
                    
                    
                    sendMessage(client, usr, str);
                    //console.log("sending to: "+usr+ " - "+str);
                });
                sendMessageDelayed(client,usr,fixedStrings['help_again'][temp['lang']], true, 35000);
            }
            else {
                if(temp['lang'] === 'en')
                    var str = fixedStrings['no_data'][temp['lang']] + itemLang;
                else
                    var str = itemLang + fixedStrings['no_data'][temp['lang']];
                sendMessage(client, usr, str);
                //console.log("sending to: "+usr+ " - "+str);
                chatState[usr]["state"] = 1;
                chatState[usr]["selection"].pop();
                setTimeout(() => {sendMenu(client, usr)}, 500);
            }
        }
        console.log("sending data2");
        setTimeout(() => {
                if(!temp['selection'].includes('Suggestions or Feedback') && !temp['selection'].includes('Provide Leads')) {
                    temp['selection'].forEach((item) => {
                        if(item in commonMessages && commonMessages[item]['en'] !== '') {
                            sendMessage(client, usr, commonMessages[item][temp['lang']]);
                        }
                    });
                }
            }, 1000);
    }
}

function changeLanguage(client, usr, lang) {

    if(!(usr in chatState)) {
        chatState[usr] = {};
        chatState[usr]['state'] = 1;
        chatState[usr]["selection"] = ['main'];
        chatState[usr]['del'] = [];
    }
    chatState[usr]['lang'] = lang;
    sendMessage(client, usr, fixedStrings['using_lang'][lang] + languageList[lang], true);
    console.log("language for " + usr + " set to " + lang);
    //send the menu
    if(chatState[usr]['state'] === 1) {
        sendMenu(client, usr);
    }
    else if(chatState[usr]['state'] === 2) {
        sendData(client, usr);
    }
}

function storeUserMessage(usr,msg) {
    sheets.spreadsheets.values.append({
        spreadsheetId: dataSpreadsheetId,
        //TODO define on top
        range: 'queryLog!G2:I',
                valueInputOption: 'USER_ENTERED',
                requestBody: {
                values: [
                    [usr.slice(0, -5), msg, + new Date()]
                ],
        }}, (err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    console.log(res.data);
  });
}

/*
 * message states
 * 0 - waiting for help
 * 1 - "help" recieved, main menu
 * 2 - menu selection done, fetching data from backend
 * 3 - data sent to client, asked for feedback
 * 4 - feedback recieved, to send to backend
 */
function start(client) {
    client.onStateChange((state) => {
        console.log('State changed: ', state);
        // force whatsapp take over
        if ('CONFLICT'.includes(state)) client.useHere();
        // detect disconnect on whatsapp
        if ('UNPAIRED'.includes(state)) console.log('logout');
    });
    // DISCONNECTED
    // SYNCING
    // RESUMING
    // CONNECTED
    let time = 0;
    client.onStreamChange((state) => {
        console.log('State Connection Stream: ' + state);
        clearTimeout(time);
        if (state === 'DISCONNECTED' || state === 'SYNCING') {
                time = setTimeout(() => {
                client.close();
            }, 80000);
        }
    });
    client.onMessage((message) => {
        //console.log('!!!sender: ', message.sender); //sender info
        //console.log('!!!from: ', message.from); //from info
        if(message.from === '918000352252@c.us' || message.from === '919824801378@c.us') {
            if(message.body.toLowerCase() === 'read'){
                getDataFromSheet(true);
            }
        }
        
        if(message.isGroupMsg === false && !(message.body.toLowerCase().includes('vcard')) ) {
            //console.log(message.body);
            var usr = message.from;
            var msg = message.body
            var t = indexOfHelpTrigger(msg.replace(/\s/g, ''));
            if(t !== -1) {
                console.log("help detected");
                if(!(usr in chatState)) {
                    chatState[usr] = {};
                    chatState[usr]['del'] = [];
                }
                if(chatState[usr]['state'] !== 3) {
                    chatState[usr]["state"] = 1;
                    chatState[usr]["selection"] = ['main'];
                    if(!('lang' in chatState[usr]) || (t !== 'en')) {
                        changeLanguage(client, usr, t);
                    }
                    else {
                        sendMenu(client, usr);
                    }
                }
            }
            
            var t = indexOfLanguageTrigger(msg.replace(/\s/g, ''));
            if(t !== -1) {
                changeLanguage(client, usr, t);
            }


            if(!isNaN(msg)) {
                if(chatState[usr]['state'] === 1) {
                    var n = +msg;
                    n = Math.floor(n);
                    var temp = chatState[usr];
                    var curItem = temp['selection'][temp['selection'].length - 1];
                    var maxN;
                    //if selection is valid
                    maxN = menuItems[curItem]['en'].length;
                    if(curItem !== 'main') {
                        maxN++;
                    }
                    if(n > 0 && n <= maxN) {
                        if(curItem !== 'main' && n === maxN) {
                            temp["selection"].pop();
                            sendMenu(client, usr);
                        } else {
                            var t2 = menuItems[curItem]['en'][n-1];
                            //append to selection
                            temp['selection'].push(t2);
                            console.log("selected "+t2);
                            //if more submenu
                            if(Object.keys(menuItems).indexOf(t2) > -1) {
                                //send menu
                                sendMenu(client, usr);
                            } else {
                                //change state and send data
                                //console.log("data state");
                                chatState[usr]["state"] = 2;
                                sendData(client, usr);
                            }
                        }
                    } else {
                        sendMessage(client, usr, fixedStrings['invalid_sel'][temp['lang']], true);
                    }
                }
            }
            else if(chatState[usr]['state'] === 3) {
                console.log("storing message");
                storeUserMessage(usr,msg);
                sendMessage(client, usr, "Thank you! our team will do the needful as soon as possible!\nSend ' *help* ' again if you need any other information");
                chatState[usr]["state"] = 1;
                chatState[usr]["selection"] = ['main'];
            }
        }
    });
}
